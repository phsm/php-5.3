```
#!bash
apt-get install autoconf2.13 apache2-prefork-dev libc-client-dev libcurl4-openssl-dev libenchant-dev libkrb5-dev libpq-dev libsnmp-dev libsqlite3-dev
apt-get source uw-imap && cd uw-imap-2007f~dfsg
export DEB_CFLAGS_MAINT_APPEND=-fPIC && dpkg-buildflags --get CFLAGS && dpkg-buildpackage -us -uc
cd .. && dpkg -i libc-client2007e_2007f~dfsg-2_amd64.deb libc-client2007e-dev_2007f~dfsg-2_amd64.deb
apt-mark hold libc-client2007e
apt-mark hold libc-client2007e-dev
ln -s /usr/lib/libc-client.a /usr/lib/x86_64-linux-gnu/libc-client.a
cd php-5.3 && dpkg-buildpackage -b -rfakeroot -us -uc

a2dismod php5 && a2dismod php52 && a2dismod php53 && a2enmod php52 && service apache2 restart
a2dismod php5 && a2dismod php52 && a2dismod php53 && a2enmod php53 && service apache2 restart
a2dismod php5 && a2dismod php52 && a2dismod php53 && a2enmod php5 && service apache2 restart
```